@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
			<p class="card-text mb-0 mt-3">{{$post->content}}</p>
			<p class="card-subtitle text-muted my-3">Likes: {{count($post->likes)}} | Comments: {{count($post->comments)}}</p>


			@if(Auth::user())
				@if(Auth::id() != $post->user_id)
					<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
						@method('PUT')
						@csrf
						@if($post->likes->contains("user_id", Auth::id()))
							<button type="submit" class="btn btn-danger">Unlike</button>
						@else
							<button type="submit" class="btn btn-success">Like</button>
						@endif
					</form>
				@endif			

				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#comment">Comment</button>

				<div class="modal fade" id="comment" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				    	<div class="modal-content">
				      		<div class="modal-header">
				        		<h5 class="modal-title" id="exampleModalLabel">Leave a comment</h5>
				        		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				      		</div>
					        <div class="modal-body">
						        <form class="d-inline" method="POST" action="/posts/{{$post->id}}/comment">
						        	@csrf
						        	<div class="mb-3">
						            	<label for="content" class="col-form-label">Comment:</label>
						            	<textarea class="form-control" name="content" id="content"></textarea>
						        	</div>

						        	<div class="modal-footer">
							        	<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
							        	<button type="sumbit" class="btn btn-primary">Comment</button>
							        </div>
						        </form>
					    	</div>
					        
					    </div>
				    </div>
				</div>

			@endif
			
			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
			</div>
		</div>
	</div>

	@if(count($postComment) == null)

		<div></div>

		@else
		<div class="mt-5">
			<h3>Comments:</h3>
			@foreach ($postComment as $comment)
				<div class="card">
					<div class="card-body text-center">
						<p class="card-text h4">{{$comment->content}}</p>
						<div class="text-end">
							<p class="card-text">Posted by: {{$comment->user->name}}</p>
							<p class="card-subtitle text-muted">posted on: {{$comment->created_at}}</p>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	@endif
@endsection