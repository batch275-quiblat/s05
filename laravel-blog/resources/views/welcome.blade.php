{{-- @extends directive lets you "extend" a template which defines its own section --}}
@extends('layouts.app')

@section('content')
    {{-- If there are already a post created, it will be displayed on our views --}}
    <div class="container text-center d-flex align-items-center justify-content-center">
        <div class="w-50 h-50">
            <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" class="img-fluid">
        </div>
    </div>
    <h3 class="my-4 text-center">Featured Posts:</h3>
    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="card text-center my-2">
                <div class="card-body">
                    <h4 class="card-title mb-3">
                        <a href="/posts/{{$post->id}}">{{$post->title}}</a>
                    </h4>
                    <h6 class="card-text mb-3">
                        Author: {{$post->user->name}}
                    </h6>
                </div>
            </div>
        @endforeach

    @else
        <div class="text-center">
            <div>There are no post to show.</div>
            <a href="/posts/create" class="btn btn-info">Create post</a>
        </div>
    @endif
@endsection
